#! /bin/bash

. base.sh

for d in $dirs; do
  echo $d:
  (cd $d && npm link)
done



all="client js-function-helpers plugin rsa-socket stack socket-initializer crane name-registry plugin-initializer stack-initializer crypto-utils client-initializer token-provider-initializer token-provider-client token-provider-client-initializer connected-emitters integration stack-io plugin-io token-provider-io"

for d in $dirs; do
  all="$all origami-$d"
done