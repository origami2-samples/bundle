#! /bin/bash

. base.sh


for d in $dirs; do
  echo $d:
  (git clone git@gitlab.com:origami2-samples/$d.git $d)
done
