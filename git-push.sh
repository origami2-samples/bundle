#! /bin/bash

. base.sh


for d in $dirs; do
  echo $d:
  (cd $d && git push)
done
