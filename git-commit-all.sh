#! /bin/bash

. base.sh


for d in $dirs; do
  echo $d:
  (cd $d && git add . --all && git commit --allow-empty -m "'$1'")
done
