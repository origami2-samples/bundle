#! /bin/bash

. base.sh


for d in $dirs; do
  echo $d:
  (cd $d && pm2 start --name $d .)
done
