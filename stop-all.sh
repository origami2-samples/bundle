#! /bin/bash

. base.sh


for d in $dirs; do
  echo $d:
  (cd $d && pm2 delete $d)
done
